#!/Usr/bin/env python3
#
# ztx roadmap
# one: get all the nodes pulled out with titles
# two: create links

import sys
import os
import argparse
from mergexp import *
from pygraphml import GraphMLParser

if __name__ == '__main__':
    input_fn = sys.argv[1]
    output_fn = sys.argv[2]
    parser = GraphMLParser()
    graph = parser.parse(input_fn)
    network_name = os.path.basename(input_fn).split('.')[0].lower()
    n = Network(network_name)
    id_table = {}
    node_table = {}
    networks = []
    for i in graph.nodes():
        node_table[i.attributes()['label'].value] = n.node(i.attributes()['label'].value)
        id_table[i.attributes()['id'].value] = i.attributes()['label'].value
    for j in graph.edges():
        networks.append(n.connect([node_table[j.node1.attributes()['label'].value], node_table[j.node2.attributes()['label'].value]]))
    n.save_json()
