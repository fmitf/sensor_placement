#!/usr/bin/env python
#
# This script is a normalization tool for the one-off task of converting topology zoo datafiles
# to a slightly more manageable format.  The zoo's datafile use this internal redirection with key values
# to store the names of node attributes.  So, for example the node's label may have the key d23 in one rendering
# and d19 in another.  The pygraphml library, in turn, doesn't do the lookup and figuring out would require going
# excessively in-depth in the XML parsing to fix, ergo a script which converts the d's into the actual attribute names.
# As a side benefit, it lowercases all the names as well to get ride of camelcase obnoxiousness.
#
# Roadmap:
# v1: outputs and dumps the labels
# v2: selects labels from command line
# v3: swaps labels
#
import argparse
import sys
import os
import pygraphml
from xml.dom.minidom import parse,parseString

def get_attributes(document):
    """
    get_attributes (document)
    Given a graphml document, extract the attributes and return a
    table of the form {id: attribute} for use in revising the document.
    """
    result = {}
    # Step 1: extract the keys
    key_list = document.getElementsByTagName('key')
    for key in key_list:
        key_name = key.getAttribute('attr.name')
        key_id = key.getAttribute('id')
        result[key_id] = key_name.lower()
    return result


def replace_keys(document, key_map):
    """
    replace_keys(document, key_map)
    Given a graphml document, replace the internal 'dblahblah' keys with their
    direct values
    """
    nodes = document.getElementsByTagName('node')
    for i in nodes:
        # We now replace the key values with the explicit
        # key names from the document front
        for elt in i.getElementsByTagName('data'):
            kn = elt.getAttribute('key')
            elt.setAttribute('key', key_map[kn])
    edges = document.getElementsByTagName('edge')
    for i in edges:
        # We now replace the key values with the explicit
        # key names from the document front
        for elt in i.getElementsByTagName('data'):
            kn = elt.getAttribute('key')
            elt.setAttribute('key', key_map[kn])
    return document

if __name__ == '__main__':
    input_fn = sys.argv[1]
    output_fn = sys.argv[2]
    document = parse(sys.argv[1])

    if len(sys.argv) > 2:
        tgts = sys.argv[2:]   
    attributes = get_attributes(parse(sys.argv[1]))
    document = replace_keys(document, attributes)
    with open(output_fn,'w') as handle:
        document.writexml(handle)
    
